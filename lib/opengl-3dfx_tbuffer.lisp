;;;; This file was automatically generated by /home/bill/programming/cl-glfw/generators/make-bindings-from-spec.lisp

(in-package #:cl-glfw-opengl) 

;;;; 3dfx_tbuffer

(defglextfun "TbufferMask3DFX" tbuffer-mask-3dfx :return "void" :args
 ((:name |mask| :type |UInt32| :direction :in)) :category "3DFX_tbuffer"
 :version "1.2") 
